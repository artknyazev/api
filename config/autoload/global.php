<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;

return [
    'doctrine' => [
        'connection'               => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
                'params'      => [
                    'host'          => 'database',
                    'port'          => 3306,
                    'user'          => getenv('MYSQL_USER'),
                    'password'      => getenv('MYSQL_PASSWORD'),
                    'dbname'        => getenv('MYSQL_DATABASE'),
                    'driverOptions' => [
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
                    ],
                ],
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache'    => 'array',
                'query_cache'       => 'array',
                'result_cache'      => 'array',
                'hydration_cache'   => 'array',
                'generate_proxies'  => true,
                'proxy_dir'         => '.cache/doctrine/proxy',
                'proxy_namespace'   => 'Doctrine\Proxy',
                'filters'           => [],
            ],
        ]
    ],
];
