<?php

namespace ApplicationTest\Service;

use Application\Entity\User;
use Application\Repository\UserRepository;
use Application\Service\UserManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\Crypt\Password\PasswordInterface;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class UserManagerTest extends AbstractHttpControllerTestCase
{

    /**
     * @covers \Application\Service\UserManager::verifyPassword
     */
    public function testVerifyPassword()
    {
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $passwordGeneratorMock = $this->createMock(PasswordInterface::class);

        $rawPassword = 'HelloWorld';
        $bcrypt = new Bcrypt();
        $encodedPassword = $bcrypt->create($rawPassword);

        $passwordGeneratorMock->expects($this->once())
            ->method('verify')
            ->with($rawPassword, $encodedPassword)
            ->willReturn(true);

        $userMock = $this->createMock(User::class);
        $userMock->method('getPassword')->willReturn($encodedPassword);

        $userManager = new UserManager($userRepositoryMock, $passwordGeneratorMock);

        $userManager->verifyPassword($rawPassword, $userMock);
    }
}
