<?php

namespace ApplicationTest\Service;

use Application\Repository\RolePermissionRepository;
use Application\Service\RolePermissionManager;
use Application\ValueObject\Role;
use Doctrine\ORM\NoResultException;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class RolePermissionManagerTest extends AbstractHttpControllerTestCase
{
    /**
     * @dataProvider providerTestCheckRoleHasPermission
     *
     * @param bool $rolePermissionFound
     * @param bool $exceptedCheckResult
     */
    public function testCheckRoleHasPermission(bool $rolePermissionFound, bool $exceptedCheckResult): void
    {
        $roleMock = $this->createMock(Role::class);
        $rolePermissionRepositoryMock = $this->createMock(RolePermissionRepository::class);
        if ($rolePermissionFound == false) {
            $rolePermissionRepositoryMock
                ->method('getRolePermissionOrExclusiveRolePermission')
                ->willThrowException(new NoResultException());
        }

        $accessManager = new RolePermissionManager($rolePermissionRepositoryMock);
        $result = $accessManager->checkRoleHasPermission($roleMock, '', '');
        $this->assertSame($exceptedCheckResult, $result);
    }

    public function providerTestCheckRoleHasPermission(): array
    {
        return [
            [true, true],
            [false, false],
        ];
    }
}
