<?php

namespace ApplicationTest\Service;

use Application\Repository\UserRepository;
use Doctrine\ORM\NoResultException;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Authentication\Result;
use Application\Entity\User;
use Application\Service\AuthenticationService;
use Application\Service\UserManager;

class AuthenticationServiceTest extends AbstractHttpControllerTestCase
{

    public function setUp()
    {
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');
        parent::setUp();
    }

    /**
     * @dataProvider providerTestAuthenticate
     *
     * @param bool $hasIdentity
     * @param string|null $username
     * @param bool|null   $userFound
     * @param bool|null   $passwordVerified
     * @param bool        $expectSessionSet
     * @param int         $expectedResultCode
     * @param bool        $expectSessionIdInResult
     */
    public function testAuthenticate(
        bool $hasIdentity,
        ?string $username,
        ?bool $userFound,
        ?bool $passwordVerified,
        bool $expectSessionSet,
        int $expectedResultCode,
        bool $expectSessionIdInResult
    ): void {
        $testSessionId = 'testSessionId';
        $sessionManagerMock = $this->createMock(SessionManager::class);
        $sessionManagerMock->method('getId')->willReturn($testSessionId);

        $sessionContainerMock = $this->createMock(Container::class);
        $sessionContainerMock->method('getManager')->willReturn($sessionManagerMock);
        $sessionContainerMock->method('offsetExists')
            ->with(AuthenticationService::SESSION_KEY)
            ->willReturn($hasIdentity);

        $userId = __LINE__;
        $userMock = $this->createMock(User::class);
        $userMock->method('getId')->willReturn($userId);
        if ($expectSessionSet) {
            $sessionContainerMock->expects($this->once())->method('offsetSet')->with($userId);
        }

        $userRepositoryMock = $this->createMock(UserRepository::class);
        if ($userFound) {
            $userRepositoryMock->method('getUserByUsername')->with($username)->willReturn($userMock);
        } else {
            $userRepositoryMock->method('getUserByUsername')->willThrowException(new NoResultException());
        }

        $userManagerMock = $this->createMock(UserManager::class);
        if (! is_null($passwordVerified)) {
            $userManagerMock->method('verifyPassword')->willReturn($passwordVerified);
        }

        $authenticationService = new AuthenticationService(
            $sessionContainerMock,
            $userManagerMock,
            $userRepositoryMock
        );
        if (! is_null($username)) {
            $authenticationService->setUsername($username);
        }
        $authenticationService->setPassword('');
        $result = $authenticationService->authenticate();

        $this->assertInstanceOf(Result::class, $result);
        $this->assertSame($expectedResultCode, $result->getCode());

        if ($expectSessionIdInResult) {
            $this->assertSame($testSessionId, $result->getIdentity());
        }
    }

    public function providerTestAuthenticate(): array
    {
        return [
              [
                  'hasIdentity' => true,
                  'username' => null,
                  'userFound' => null,
                  'passwordVerified' => null,
                  'expectSessionSet' => false,
                  'expectedResultCode' => Result::SUCCESS,
                  'expectSessionIdInResult' => false,
              ],
              [
                  'hasIdentity' => false,
                  'username' => null,
                  'userFound' => null,
                  'passwordVerified' => null,
                  'expectSessionSet' => false,
                  'expectedResultCode' => Result::FAILURE_IDENTITY_AMBIGUOUS,
                  'expectSessionIdInResult' => false,
              ],
              [
                  'hasIdentity' => false,
                  'username' => 'test',
                  'userFound' => false,
                  'passwordVerified' => null,
                  'expectSessionSet' => false,
                  'expectedResultCode' => Result::FAILURE_IDENTITY_NOT_FOUND,
                  'expectSessionIdInResult' => false,
              ],
              [
                  'hasIdentity' => false,
                  'username' => 'test',
                  'userFound' => true,
                  'passwordVerified' => false,
                  'expectSessionSet' => false,
                  'expectedResultCode' => Result::FAILURE_CREDENTIAL_INVALID,
                  'expectSessionIdInResult' => false,
              ],
              [
                  'hasIdentity' => false,
                  'username' => 'test',
                  'userFound' => true,
                  'passwordVerified' => true,
                  'expectSessionSet' => false,
                  'expectedResultCode' => Result::SUCCESS,
                  'expectSessionIdInResult' => false,
              ],
        ];
    }

    public function testHasIdentity(): void
    {
        $testResultHasIdentity = true;
        $sessionContainerMock = $this->createMock(Container::class);
        $sessionContainerMock->expects($this->once())
            ->method('offsetExists')
            ->with(AuthenticationService::SESSION_KEY)
            ->willReturn($testResultHasIdentity);

        $userManagerMock = $this->createMock(UserManager::class);
        $userRepositoryMock = $this->createMock(UserRepository::class);

        $authenticationService = new AuthenticationService(
            $sessionContainerMock,
            $userManagerMock,
            $userRepositoryMock
        );
        $result = $authenticationService->hasIdentity();
        $this->assertSame($testResultHasIdentity, $result);
    }

    public function testGetIdentity(): void
    {
        $testIdentity = __LINE__;
        $sessionContainerMock = $this->createMock(Container::class);
        $sessionContainerMock->expects($this->once())
            ->method('offsetGet')
            ->with(AuthenticationService::SESSION_KEY)
            ->willReturn($testIdentity);

        $userManagerMock = $this->createMock(UserManager::class);
        $userRepositoryMock = $this->createMock(UserRepository::class);

        $authenticationService = new AuthenticationService(
            $sessionContainerMock,
            $userManagerMock,
            $userRepositoryMock
        );
        $identity = $authenticationService->getIdentity();
        $this->assertSame($testIdentity, $identity);
    }

    public function testClearIdentity(): void
    {
        $sessionManagerMock = $this->createMock(SessionManager::class);
        $sessionManagerMock->expects($this->once())->method('destroy');

        $sessionContainerMock = $this->createMock(Container::class);
        $sessionContainerMock->method('getManager')->willReturn($sessionManagerMock);

        $userManagerMock = $this->createMock(UserManager::class);
        $userRepositoryMock = $this->createMock(UserRepository::class);

        $authenticationService = new AuthenticationService(
            $sessionContainerMock,
            $userManagerMock,
            $userRepositoryMock
        );
        $authenticationService->clearIdentity();
    }
}
