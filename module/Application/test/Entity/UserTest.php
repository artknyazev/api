<?php

namespace ApplicationTest\Entity;

use Application\Entity\User;
use Application\ValueObject\Role;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Тестирование модели пользователей
 * Class UserTest
 *
 * @package ApplicationTest\Entity
 */
class UserTest extends AbstractHttpControllerTestCase
{

    /**
     * Тест записи и чтения свойств модели
     *
     * @dataProvider providerTestUserProperties
     *
     * @param int    $id
     * @param string $username
     * @param string $password
     * @param Role   $role
     */
    public function testGettersAndSetters(int $id, string $username, string $password, Role $role)
    {
        $user = new User();
        $user->setId($id)
            ->setUsername($username)
            ->setPassword($password)
            ->setRole($role);

        $this->assertSame($id, $user->getId());
        $this->assertSame($username, $user->getUsername());
        $this->assertSame($password, $user->getPassword());
        $this->assertSame($role, $user->getRole());
        $this->assertInstanceOf(Role::class, $role);
    }

    /**
     * Провайдер данных для метода testUserProperties
     */
    public function providerTestUserProperties(): array
    {
        $roleMock = $this->createMock(Role::class);

        return [
            [
                1, 'Вася Пупкин', 'password', $roleMock
            ],
        ];
    }
}
