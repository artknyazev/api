<?php

namespace ApplicationTest\ValueObject;

use Application\ValueObject\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Тесты для класса Role
 * Class RoleTest
 *
 * @package ApplicationTest\ValueObject
 */
class RoleTest extends AbstractHttpControllerTestCase
{
    /**
     * @dataProvider providerTestConstructorAndGetters
     *
     * @param int    $id
     * @param string $name
     */
    public function testConstructorAndGetters(int $id, string $name)
    {
        $role = new Role($id, $name);

        $this->assertSame($id, $role->getId());
        $this->assertSame($name, $role->getName());
        $this->assertInstanceOf(ArrayCollection::class, $role->getPermissions());
    }

    /**
     * @return array
     */
    public function providerTestConstructorAndGetters(): array
    {
        return [
            [0, 'Гость из будущего'],
            [1, 'Админ из прошлого'],
        ];
    }
}
