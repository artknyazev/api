<?php

namespace ApplicationTest\ValueObject;

use Application\ValueObject\Role;
use Application\ValueObject\RolePermission;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class RolePermissionTest
 *
 * @package ApplicationTest\ValueObject
 */
class RolePermissionTest extends AbstractHttpControllerTestCase
{
    /**
     * @dataProvider providerTestConstructorAndGetters
     *
     * @param Role   $role
     * @param string $controller
     * @param string $action
     */
    public function testConstructorAndGetters(Role $role, string $controller, string $action)
    {
        $rolePermission = new RolePermission($role, $controller, $action);

        $this->assertSame($role, $rolePermission->getRole());
        $this->assertSame($controller, $rolePermission->getController());
        $this->assertSame($action, $rolePermission->getAction());
        $this->assertInstanceOf(Role::class, $rolePermission->getRole());
    }

    /**
     * @return array
     */
    public function providerTestConstructorAndGetters(): array
    {
        $role = new Role(1, 'Гость из будущего');

        return [
            [$role, 'site', 'index'],
        ];
    }
}
