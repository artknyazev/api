<?php

namespace ApplicationTest;

use Doctrine\ORM\EntityManager;
use Zend\Test\PHPUnit\Controller\AbstractControllerTestCase;

/**
 * Абстрактный класс, от которого необходимо наследовать классы тестов с включением транзакций БД
 * Class AbstractTransactionalTestCase
 *
 * @package ApplicationTest
 */
abstract class AbstractTransactionalTestCase extends AbstractControllerTestCase
{
    /** @var EntityManager */
    protected $entityManager;

    public function setUp()
    {
        parent::setUp();

        $this->entityManager = $this->getApplicationServiceLocator()->get(EntityManager::class);

        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
    }

    public function tearDown()
    {
        if ($this->entityManager->getConnection()->isTransactionActive()) {
            $this->entityManager->getConnection()->rollBack();
        }

        parent::tearDown();
    }
}
