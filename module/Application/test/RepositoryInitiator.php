<?php

namespace ApplicationTest;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Трейт с функционалом, выполняющимся перед запуском тестов репозиториев
 *
 * @package ApplicationTest
 */
trait RepositoryInitiator
{
    /**
     * @var ORMExecutor
     */
    protected $ormExecutor;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Инициализация соединения с БД и репозитория для тестирования
     *
     * @param string $repositoryClass
     * @param array  $exceptedEntities
     */
    public function initRepository(string $repositoryClass, array $exceptedEntities = []): void
    {
        $this->entityManager = $this->getApplicationServiceLocator()->get(EntityManager::class);

        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);

        $ormPurger = new ORMPurger($this->entityManager, $exceptedEntities);
        $ormPurger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);
        $this->ormExecutor = new ORMExecutor($this->entityManager, $ormPurger);
        $this->repository = $this->entityManager->getRepository($repositoryClass);
    }
}
