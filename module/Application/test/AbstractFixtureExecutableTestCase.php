<?php

namespace ApplicationTest;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityRepository;

/**
 * Абстрактный класс, от которого наследуются классы тестов, вызывающих фикстуры
 * Class AbstractFixtureExecutableTestCase
 *
 * @package ApplicationTest
 */
abstract class AbstractFixtureExecutableTestCase extends AbstractTransactionalTestCase
{

    /** @var  ORMPurger */
    protected $purger;

    /** @var ORMExecutor */
    protected $ormExecutor;

    /** @var EntityRepository */
    protected $repository;

    /**
     * Установка очистителя БД
     *
     * @param mixed $purger
     *
     * @return AbstractFixtureExecutableTestCase
     */
    public function setPurger(ORMPurger $purger): self
    {
        $this->purger = $purger;
        $this->purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);

        return $this;
    }

    /**
     * Применение массива фикстур
     *
     * @param FixtureInterface[] $fixtures
     *
     * @throws \Exception
     */
    public function executeFixtures(array $fixtures): void
    {
        if (! isset($this->purger)) {
            throw new \Exception('DB purger is not defined');
        }
        $this->ormExecutor = new ORMExecutor($this->entityManager, $this->purger);
        $this->ormExecutor->execute($fixtures);
    }
}
