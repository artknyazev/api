<?php

namespace ApplicationTest\Repository\Fixtures;

use Application\Entity\User;
use Application\Repository\RoleRepository;
use Application\ValueObject\Role;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class FindUserByUsernameFixture implements FixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var RoleRepository $roleRepository */
        $roleRepository = $manager->getRepository(Role::class);
        $role = $roleRepository->getGuestRole();

        $userNames = [
            'UserShouldBeFound',
            'OneMoreUserShouldBeFound',
        ];

        foreach ($userNames as $userName) {
            $user = new User();
            $user->setUsername($userName);
            $user->setPassword('');
            $user->setRole($role);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
