<?php

namespace ApplicationTest\Repository\Fixtures;

use Application\Repository\RoleRepository;
use Application\ValueObject\Role;
use Application\ValueObject\RolePermission;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GetRolePermissionOrExclusiveRolePermissionFixture implements FixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var RoleRepository $roleRepository */
        $roleRepository = $manager->getRepository(Role::class);
        $guestRole = $roleRepository->getGuestRole();
        $adminRole = $roleRepository->getAdminRole();

        $permissions = [
            [
                'role'       => $guestRole,
                'controller' => 'authentication',
                'action'     => 'signin',
            ],
            [
                'role'       => $adminRole,
                'controller' => '*',
                'action'     => '*',
            ],
        ];

        foreach ($permissions as $permission) {
            $rolePermission = new RolePermission($permission['role'], $permission['controller'], $permission['action']);
            $manager->persist($rolePermission);
        }

        $manager->flush();
    }
}
