<?php

namespace ApplicationTest\Repository\Fixtures;

use Application\ValueObject\Role;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Фикстура для тестирования ролей
 * Class RolesFixture
 *
 * @package ApplicationTest\Repository\Fixtures
 */
class RolesFixture implements FixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $rolesData = [
            [
                'id'   => Role::GUEST_ROLE_ID,
                'name' => 'Guest',
            ],
            [
                'id'   => Role::ADMIN_ROLE_ID,
                'name' => 'Manager',
            ],
        ];

        foreach ($rolesData as $roleData) {
            $role = new Role($roleData['id'], $roleData['name']);
            $manager->persist($role);
        }

        $manager->flush();
    }
}
