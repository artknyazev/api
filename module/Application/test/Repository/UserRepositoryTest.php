<?php declare(strict_types=1);

namespace ApplicationTest\Repository;

use Application\Entity\User;
use ApplicationTest\AbstractFixtureExecutableTestCase;
use ApplicationTest\Repository\Fixtures\FindUserByUsernameFixture;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class UserRepositoryTest extends AbstractFixtureExecutableTestCase
{

    public function setUp(): void
    {
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');
        parent::setUp();

        $ormPurger = new ORMPurger($this->entityManager, ['roles']);
        $this->setPurger($ormPurger);
        $this->repository = $this->entityManager->getRepository(User::class);
    }

    /**
     * @dataProvider providerTestFindOneByUserName
     *
     * @param string      $userName
     * @param string      $expectedException
     * @param string|null $expectedName
     *
     * @throws \Exception
     * @covers       \Application\Repository\UserRepository::getUserByUsername
     */
    public function testGetUserByUsername(
        string $userName,
        string $expectedException,
        string $expectedName = null
    ): void {
        $this->executeFixtures([new FindUserByUsernameFixture()]);

        if ($expectedException) {
            $this->expectException($expectedException);
        }

        $user = $this->repository->getUserByUsername($userName);
        $this->assertInstanceOf(User::class, $user);
        $this->assertSame($expectedName, $user->getUsername());
    }

    public function providerTestFindOneByUserName(): array
    {
        return [
            ['UserShouldBeFound', false, 'UserShouldBeFound'],
            ['OneMoreUserShouldBeFound', false, 'OneMoreUserShouldBeFound'],
            ['NonexistentUser', \Doctrine\ORM\NoResultException::class, null],
        ];
    }
}
