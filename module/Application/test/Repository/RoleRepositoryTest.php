<?php

namespace ApplicationTest\Repository;

use Application\ValueObject\Role;
use ApplicationTest\AbstractFixtureExecutableTestCase;
use ApplicationTest\Repository\Fixtures\RolesFixture;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class RoleRepositoryTest extends AbstractFixtureExecutableTestCase
{

    public function setUp(): void
    {
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');
        parent::setUp();

        $ormPurger = new ORMPurger($this->entityManager);
        $this->setPurger($ormPurger);
        $this->repository = $this->entityManager->getRepository(Role::class);
    }

    /**
     *  Тест получения роли гостя
     *
     * @throws \Exception
     * @covers \Application\Repository\RoleRepository::getGuestRole
     */
    public function testGetGuestRole()
    {
        $this->executeFixtures([new RolesFixture()]);

        $guestRole = $this->repository->getGuestRole();
        $this->assertInstanceOf(Role::class, $guestRole);
        $this->assertSame('Guest', $guestRole->getName());
    }

    /**
     *  Тест получения роли админа
     *
     * @throws \Exception
     * @covers \Application\Repository\RoleRepository::getAdminRole
     */
    public function testGetAdminRole()
    {
        $this->executeFixtures([new RolesFixture()]);

        $adminRole = $this->repository->getAdminRole();
        $this->assertInstanceOf(Role::class, $adminRole);
        $this->assertSame('Manager', $adminRole->getName());
    }
}
