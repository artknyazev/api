<?php declare(strict_types=1);

namespace ApplicationTest\Repository;

use Application\ValueObject\RolePermission;
use ApplicationTest\AbstractFixtureExecutableTestCase;
use ApplicationTest\Repository\Fixtures\GetRolePermissionOrExclusiveRolePermissionFixture;
use ApplicationTest\RepositoryInitiator;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Тестирование репозитория прав доступа пользователей
 * Class RolePermissionRepositoryTest
 *
 * @package ApplicationTest\Repository
 */
class RolePermissionRepositoryTest extends AbstractFixtureExecutableTestCase
{

    /**
     * Подготовка к тесту
     */
    public function setUp()
    {
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');
        parent::setUp();

        $ormPurger = new ORMPurger($this->entityManager, ['roles']);
        $this->setPurger($ormPurger);
        $this->repository = $this->entityManager->getRepository(RolePermission::class);
    }

    /**
     * Тестирование получения правила доступа по параметрам
     *
     * @dataProvider providerTestGetRolePermissionOrExclusiveRolePermission
     *
     * @param int         $roleId
     * @param string      $controller
     * @param string      $action
     * @param string      $expectedController
     * @param string      $expectedAction
     * @param string|null $expectedException
     *
     * @throws \Exception
     * @covers       \Application\Repository\RolePermissionRepository::getRolePermissionOrExclusiveRolePermission
     */
    public function testGetRolePermissionOrExclusiveRolePermission(
        int $roleId,
        string $controller,
        string $action,
        string $expectedController,
        string $expectedAction,
        $expectedException
    ) {
        $this->executeFixtures([new GetRolePermissionOrExclusiveRolePermissionFixture()]);

        if ($expectedException) {
            $this->expectException($expectedException);
        }

        $rolePermission = $this->repository->getRolePermissionOrExclusiveRolePermission($roleId, $controller, $action);
        $this->assertInstanceOf(RolePermission::class, $rolePermission);
        $this->assertSame($roleId, $rolePermission->getRole()->getId());
        $this->assertSame($expectedController, $rolePermission->getController());
        $this->assertSame($expectedAction, $rolePermission->getAction());
    }

    /**
     * Провайдер для метода testGetRolePermissionOrExclusiveRolePermission
     *
     * @return array
     */
    public function providerTestGetRolePermissionOrExclusiveRolePermission(): array
    {
        return [
            'Existing record guest'                    => [
                0,
                'authentication',
                'signin',
                'authentication',
                'signin',
                null,
            ],
            'Existing record guest, no access to blog' => [
                0,
                'blog',
                'post',
                '',
                '',
                \Doctrine\ORM\NoResultException::class,
            ],
            'Existing record admin'                    => [
                1,
                'blog',
                'post',
                '*',
                '*',
                null,
            ],
            'Nonexisting role'                         => [
                -1,
                'authentication',
                'signin',
                '',
                '',
                \Doctrine\ORM\NoResultException::class,
            ],
        ];
    }
}
