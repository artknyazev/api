<?php

namespace ApplicationTest\Controller;

use Application\Controller\AuthenticationController;
use Application\Service\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\Parameters;

class AuthenticationControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp(): void
    {
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');
        parent::setUp();
    }

    public function testSigninRoute(): void
    {
        $this->dispatch('/authentication/signin', 'POST');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(AuthenticationController::class);
        $this->assertMatchedRouteName('signin');
    }

    /**
     * @dataProvider providerSigninAction
     *
     * @param string $username
     * @param string $password
     * @param Result $authenticationResult
     * @param array  $requiredResponseValues
     */
    public function testSigninAction(
        string $username,
        string $password,
        Result $authenticationResult,
        array $requiredResponseValues
    ): void {
        $authenticationServiceMock = $this->createMock(AuthenticationService::class);
        $authenticationServiceMock->expects($this->once())->method('setUsername')->with($username);
        $authenticationServiceMock->expects($this->once())->method('setPassword')->with($password);

        $authenticationServiceMock->method('authenticate')->willReturn($authenticationResult);
        $authenticationServiceMock->expects($this->at(2))->method('authenticate');

        $authenticationController = new AuthenticationController($authenticationServiceMock);
        $postParams = new Parameters(['username' => $username, 'password' => $password]);
        $authenticationController->getRequest()->setPost($postParams);
        $response = $authenticationController->signinAction();

        $this->assertArraySubset($requiredResponseValues, $response);
    }

    public function providerSigninAction(): array
    {
        return [
            [
                'uname',
                'pass',
                new Result(Result::FAILURE, null),
                ['result' => false],
            ],
            [
                'uname2',
                'pass2',
                new Result(Result::FAILURE_CREDENTIAL_INVALID, null),
                ['result' => false],
            ],
            [
                'uname3',
                'pass3',
                new Result(Result::SUCCESS, 'sess_id'),
                ['result' => true, 'token' => 'sess_id'],
            ],
        ];
    }

    public function testSignoutRoute(): void
    {
        $this->dispatch('/authentication/signout', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(AuthenticationController::class);
        $this->assertMatchedRouteName('signout');
    }

    public function testSignoutAction(): void
    {
        $authenticationServiceMock = $this->createMock(AuthenticationService::class);
        $authenticationServiceMock->expects($this->once())->method('clearIdentity');

        $authenticationController = new AuthenticationController($authenticationServiceMock);
        $response = $authenticationController->signoutAction();
        $this->assertArrayHasKey('result', $response);
        $this->assertTrue($response['result']);
    }
}
