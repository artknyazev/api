<?php

declare(strict_types=1);

namespace Application\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190118134728 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Базовые права доступа ролей';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<EOF
CREATE TABLE roles_permissions (
    role_id INT UNSIGNED NOT NULL,
    controller VARCHAR(75) NOT NULL,
    action VARCHAR(75) NOT NULL,
    PRIMARY KEY(role_id, controller, action)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
EOF
        );
    }

    public function postUp(Schema $schema): void
    {
        $this->connection->exec(<<<EOF
INSERT INTO
  roles_permissions (role_id, controller, action) 
VALUES
  (0, 'index', 'index'),
  (0, 'authentication', 'signin'),  
  (1, '*', '*');
EOF
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE IF EXISTS roles_permissions;');
    }
}
