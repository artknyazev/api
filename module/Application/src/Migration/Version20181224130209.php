<?php declare(strict_types=1);

namespace Application\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181224130209 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Пользователи и базовые роли';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<EOF
CREATE TABLE users (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role_id INT UNSIGNED NOT NULL,
    UNIQUE INDEX username_idx (username),
    PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
EOF
        );

        $this->addSql(<<<EOF
CREATE TABLE roles (
    id INT UNSIGNED NOT NULL,
    name VARCHAR(75) NOT NULL,
    PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
EOF
        );
    }

    public function postUp(Schema $schema): void
    {
        $this->connection->exec(<<<EOF
INSERT INTO
  roles (id, name) 
VALUES
  (0, 'guest'),
  (1, 'admin');
EOF
        );

        $this->connection->exec(<<<EOF
INSERT INTO
    users (username, password, role_id) 
VALUES
    ('user', '$2a$10\$QdgdeKzwa0EamORiHO4MnO8P9.hNgw8jJ9hsEfybI1bDFl7dKDN4K', 0),
    ('admin', '$2a$10\$QdgdeKzwa0EamORiHO4MnO8P9.hNgw8jJ9hsEfybI1bDFl7dKDN4K', 1);
EOF
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE users;');
        $this->addSql('DROP TABLE roles;');
    }
}
