<?php

namespace Application\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\RolePermissionRepository", readOnly=true)
 * @ORM\Table(name="roles_permissions")
 */
class RolePermission
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="permissions", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=false)
     */
    private $controller;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=false)
     */
    private $action;

    /**
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * RolePermission constructor.
     *
     * @param Role   $role
     * @param string $controller
     * @param string $action
     */
    public function __construct(Role $role, string $controller, string $action)
    {
        $this->role = $role;
        $this->controller = $controller;
        $this->action = $action;
    }
}
