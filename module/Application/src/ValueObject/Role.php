<?php

namespace Application\ValueObject;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Repository\RoleRepository", readOnly=true)
 * @ORM\Table(name="roles")
 */
class Role
{
    /**
     * Идентификатор гостевой роли (роль по умолчанию)
     */
    public const GUEST_ROLE_ID = 0;

    /**
     * Идентификатор админской роли
     */
    public const ADMIN_ROLE_ID = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer", nullable=false, options={"unsigned":true})
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="RolePermission", mappedBy="role", fetch="EXTRA_LAZY")
     */
    private $permissions;

    /**
     * Role constructor.
     *
     * @param integer $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->permissions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection
     */
    public function getPermissions(): Collection
    {
        return $this->permissions;
    }
}
