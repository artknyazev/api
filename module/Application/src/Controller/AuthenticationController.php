<?php

namespace Application\Controller;

use Zend\Authentication\Result;
use Zend\Mvc\Controller\AbstractActionController;
use Application\Service\AuthenticationService;

/**
 * Class AuthenticationController
 *
 * @package Application\Controller
 */
class AuthenticationController extends AbstractActionController
{
    /**
     * @var AuthenticationService
     */
    private $authService;

    /**
     * AuthenticationController constructor.
     *
     * @param AuthenticationService $authService
     */
    public function __construct(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Вход по имени пользователя и паролю
     *
     * @return array
     */
    public function signinAction(): array
    {
        $this->authService->setUsername($this->params()->fromPost('username', ''));
        $this->authService->setPassword($this->params()->fromPost('password', ''));

        $result = $this->authService->authenticate();

        if ($result->getCode() !== Result::SUCCESS) {
            return [
                'result'   => false,
                'messages' => $result->getMessages(),
            ];
        }

        return [
            'result'      => true,
            'messages'    => $result->getMessages(),
            'token'       => $result->getIdentity(),
        ];
    }

    /**
     * Выход пользователя
     *
     * @return array
     */
    public function signoutAction(): array
    {
        $this->authService->clearIdentity();

        return ['result' => true];
    }
}
