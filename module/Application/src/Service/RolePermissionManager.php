<?php

namespace Application\Service;

use Application\Repository\RolePermissionRepository;
use Application\ValueObject\Role;
use Application\ValueObject\RolePermission;

/**
 * Class AccessManager
 *
 * @package Application\Service
 */
class RolePermissionManager
{
    /**
     * @var RolePermissionRepository
     */
    private $repository;

    /**
     * AccessManager constructor.
     *
     * @param RolePermissionRepository $repository
     */
    public function __construct(RolePermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Проверяет, что роль имеет право доступа (или эксклюзивное право "*") к контроллеру/экшену
     *
     * @param Role   $role
     * @param string $controllerClass
     * @param string $action
     *
     * @return bool
     */
    public function checkRoleHasPermission(Role $role, string $controllerClass, string $action): bool
    {
        try {
            $rolePermission = $this->repository->getRolePermissionOrExclusiveRolePermission(
                $role->getId(),
                $controllerClass,
                $action
            );

            return $rolePermission instanceof RolePermission;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
