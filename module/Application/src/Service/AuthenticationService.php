<?php

namespace Application\Service;

use Application\Repository\UserRepository;
use Zend\Authentication\Result;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Session\Container;

/**
 * Class AuthenticationService
 *
 * @package Application\Service
 */
class AuthenticationService implements AuthenticationServiceInterface
{
    /**
     * Ключ контейнера сессии, в котором хранится идентификатор авторизованного пользователя
     */
    public const SESSION_KEY = 'user_id';

    /**
     * @var Container
     */
    protected $sessionContainer;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * AuthenticationService constructor.
     *
     * @param Container      $sessionContainer
     * @param UserManager    $userManager
     * @param UserRepository $userRepository
     */
    public function __construct(Container $sessionContainer, UserManager $userManager, UserRepository $userRepository)
    {
        $this->sessionContainer = $sessionContainer;
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
    }

    /**
     * Установка имени пользователя для авторизации
     *
     * @param string $username
     *
     * @return AuthenticationService
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Установка нехешированного пароля для авторизации
     *
     * @param string $password
     *
     * @return AuthenticationService
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Метод для проверки логина и пароля пользователя, в случае успешной аутентификации устанавливает сессию
     *
     * @return Result
     */
    public function authenticate(): Result
    {
        if ($this->hasIdentity()) {
            return new Result(
                Result::SUCCESS,
                $this->sessionContainer->getManager()->getId(),
                ['Already authenticated']
            );
        }

        if (! $this->username) {
            return new Result(Result::FAILURE_IDENTITY_AMBIGUOUS, null, ['Username is ambiguous']);
        }

        try {
            $user = $this->userRepository->getUserByUsername($this->username);

            if (! $this->userManager->verifyPassword($this->password, $user)) {
                return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, ['Invalid credentials']);
            }

            // В случае успешной авторизации пишем в сессию уникальный идентификатор пользователя
            $this->sessionContainer->offsetSet(self::SESSION_KEY, $user->getId());

            // В результате возвращаем уникальный идентификатор сессии
            return new Result(
                Result::SUCCESS,
                $this->sessionContainer->getManager()->getId(),
                ['Authenticated successfully']
            );
        } catch (\Throwable $exception) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, ['Invalid credentials']);
        }
    }

    /**
     * Проверка наличия идентификатора в сессии
     *
     * @return bool - true, если есть, false, если нет
     */
    public function hasIdentity(): bool
    {
        return $this->sessionContainer->offsetExists(self::SESSION_KEY);
    }

    /**
     * Получение идентификатора пользователя из сессии
     *
     * @return int|null
     */
    public function getIdentity(): ?int
    {
        return (int)$this->sessionContainer->offsetGet(self::SESSION_KEY);
    }

    /**
     * Завершение (удаление) сессии
     *
     * @return void
     */
    public function clearIdentity(): void
    {
        $this->sessionContainer->getManager()->destroy();
    }
}
