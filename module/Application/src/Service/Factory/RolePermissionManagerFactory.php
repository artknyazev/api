<?php

namespace Application\Service\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\RolePermissionManager;
use Application\ValueObject\RolePermission;

/**
 * Class AccessManagerFactory
 *
 * @package Application\Service\Factory
 */
class RolePermissionManagerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return RolePermissionManager|object
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): RolePermissionManager {
        $rolePermissionRepository = $container->get(EntityManager::class)->getRepository(RolePermission::class);

        return new RolePermissionManager($rolePermissionRepository);
    }
}
