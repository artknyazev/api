<?php

namespace Application\Service\Factory;

use Application\Entity\User;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\AuthenticationService;

/**
 * Class AuthenticationServiceFactory
 *
 * @package Application\Service\Factory
 */
class AuthenticationServiceFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return AuthenticationService|object
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): AuthenticationService {
        $sessionContainer = $container->get('AuthenticationContainer');
        $userManager = $container->get(UserManager::class);
        $userRepository = $container->get(EntityManager::class)->getRepository(User::class);

        return new AuthenticationService($sessionContainer, $userManager, $userRepository);
    }
}
