<?php

namespace Application\Service\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Entity\User;
use Application\Service\UserManager;

/**
 * Class UserManagerFactory
 *
 * @package Application\Service\Factory
 */
class UserManagerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return UserManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserManager
    {
        $userRepository = $container->get(EntityManager::class)->getRepository(User::class);
        $bcrypt = new Bcrypt();

        return new UserManager($userRepository, $bcrypt);
    }
}
