<?php

namespace Application\Service;

use Application\Repository\UserRepository;
use Application\Entity\User;
use Zend\Crypt\Password\PasswordInterface;

/**
 * Class UserManager
 *
 * @package Application\Service
 */
class UserManager
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var PasswordInterface
     */
    private $passwordGenerator;

    /**
     * UserManager constructor.
     *
     * @param UserRepository    $repository
     * @param PasswordInterface $passwordGenerator
     */
    public function __construct(UserRepository $repository, PasswordInterface $passwordGenerator)
    {
        $this->repository = $repository;
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * Сверка нехешированного пароля с хешированным значением пароля пользователя
     *
     * @param string $password
     * @param User   $user
     *
     * @return bool
     */
    public function verifyPassword(string $password, User $user): bool
    {
        return $this->passwordGenerator->verify($password, $user->getPassword());
    }
}
