<?php

namespace Application\Repository;

use Application\ValueObject\Role;
use Application\ValueObject\RolePermission;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;

/**
 * Class RolePermissionRepository
 *
 * @package Application\Repository
 */
class RolePermissionRepository extends EntityRepository
{
    /**
     * Поиск конкретного или эксклюзивного права ("*") доступа для роли
     *
     * @param int    $roleId
     * @param string $controllerClass
     * @param string $action
     *
     * @return RolePermission
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRolePermissionOrExclusiveRolePermission(
        int $roleId,
        string $controllerClass,
        string $action
    ): RolePermission {

        $query = $this->createQueryBuilder('rp')
            ->andWhere('rp.role = :role_id')
            ->andWhere('rp.controller IN (:controller)')
            ->andWhere('rp.action IN (:action)')
            ->setParameter('role_id', $roleId)
            ->setParameter('controller', ['*', $controllerClass])
            ->setParameter('action', ['*', $action])
            ->getQuery();

        return $query->getSingleResult();
    }
}
