<?php

namespace Application\Repository;

use Application\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 *
 * @package Application\Repository
 */
class UserRepository extends EntityRepository
{

    /**
     * Получение пользователя по username
     *
     * @param string $username
     *
     * @return User
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserByUsername(string $username): User
    {
        $query = $this->createQueryBuilder('u')
            ->where('u.username = ?1')
            ->setParameter(1, $username)
            ->getQuery();

        return $query->getSingleResult();
    }
}
