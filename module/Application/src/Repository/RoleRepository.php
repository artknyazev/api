<?php

namespace Application\Repository;

use Application\ValueObject\Role;
use Doctrine\ORM\EntityRepository;

/**
 * Class RoleRepository
 *
 * @package Application\Repository
 */
class RoleRepository extends EntityRepository
{
    /**
     * Получение гостевой роли
     *
     * @return Role
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getGuestRole(): Role
    {
        $query = $this->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', Role::GUEST_ROLE_ID)
            ->getQuery();

        return $query->getSingleResult();
    }

    /**
     * Получение админской роли
     *
     * @return Role
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAdminRole(): Role
    {
        $query = $this->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', Role::ADMIN_ROLE_ID)
            ->getQuery();

        return $query->getSingleResult();
    }
}
