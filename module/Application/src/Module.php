<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\View\Model\JsonModel;
use Zend\Session;

class Module
{
    /**
     * @return array
     */
    public function getConfig(): array
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event): void
    {
        $application = $event->getApplication();
        $eventManager = $application->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'onRender'], 100);
    }

    /**
     * Событие рендеринга для перехвата вывода. Во всех случаях должна использоваться JsonModel
     *
     * @param MvcEvent $event
     */
    public function onRender(MvcEvent $event): void
    {
        $viewModel = $event->getResult();
        if (! ($viewModel instanceof JsonModel)) {
            if ($event->isError()) {
                $variables = [
                    'result'   => false,
                    'messages' => [$viewModel->getVariable('message')],
                ];

                // Если в конфиге прописана опция отображения исключений
                if ($viewModel->getVariable('display_exceptions')) {
                    $exception = $viewModel->getVariable('exception');
                    if ($exception instanceof \Throwable) {
                        $variables['exception'] = [
                            'code'    => $exception->getCode(),
                            'message' => $exception->getMessage(),
                            'file'    => $exception->getFile(),
                            'line'    => $exception->getLine(),
                            'trace'   => $exception->getTrace(),
                        ];
                    }
                }
            } else {
                $variables = $viewModel->getVariables();
            }

            $jsonViewModel = new JsonModel($variables);
            $jsonViewModel->setTerminal(true);
            // Форматированный вывод для исключений
            if ($viewModel->getVariable('display_exceptions')) {
                $jsonViewModel->setOption('prettyPrint', true);
            }

            $event->setResult($jsonViewModel);
            $event->setViewModel($jsonViewModel);
        }
    }
}
