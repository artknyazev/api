<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Session;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router'             => [
        'routes' => [
            'home'    => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'signin'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/authentication/signin',
                    'verb'     => 'post',
                    'defaults' => [
                        'controller' => Controller\AuthenticationController::class,
                        'action'     => 'signin',
                    ],
                ],
            ],
            'signout' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/authentication/signout',
                    'verb'     => 'get',
                    'defaults' => [
                        'controller' => Controller\AuthenticationController::class,
                        'action'     => 'signout',
                    ],
                ],
            ],
        ],
    ],
    'controllers'        => [
        'factories' => [
            Controller\IndexController::class          => InvokableFactory::class,
            Controller\AuthenticationController::class => Controller\Factory\AuthenticationControllerFactory::class,
        ],
    ],
    'service_manager'    => [
        'factories' => [
            Service\UserManager::class           => Service\Factory\UserManagerFactory::class,
            Service\RolePermissionManager::class => Service\Factory\RolePermissionManagerFactory::class,
            Service\AuthenticationService::class => Service\Factory\AuthenticationServiceFactory::class,
        ],
    ],
    'view_manager'       => [
        'display_not_found_reason' => true,
        'display_exceptions'       => false,
        'strategies'               => [
            'ViewJsonStrategy',
        ],
    ],
    'session_config'     => [
        'config_class'     => Session\Config\SessionConfig::class,
        'cookie_lifetime'  => 60 * 60 * 1,
        'gc_maxlifetime'   => 60 * 60 * 24,
        'use_cookies'      => false,
        'use_only_cookies' => false,
        'name'             => 'token',
    ],
    'session_storage'    => [
        'type' => Session\Storage\SessionArrayStorage::class,
    ],
    'session_containers' => [
        'AuthenticationContainer',
    ],
    'doctrine'           => [
        'driver'                   => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                    __DIR__ . '/../src/ValueObject',
                ],
            ],
            'orm_default'             => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => __DIR__ . '/../src/Migration',
                'name'      => 'Application Migrations',
                'namespace' => 'Application\Migration',
                'table'     => 'migrations',
            ],
        ],
    ],
];
